﻿readme
=> 3 containers NGINX PHP MARIADB


Création d’un réseau pour les 3 containers 


docker network create reseau


On commence par le container MARIADB parce que nous avons besoin de lui pour construire le PHP et du PHP pour NGINX


docker run --rm -d --name bdd --network reseau -v ./data:/var/lib/mysql -e MARIADB_USER=user -e MARIADB_PASSWORD=motdepasse -e MARIADB_DATABASE=bdd_wp -e MARIADB_ROOT_PASSWORD=motdepasse  mariadb:11.2.2


-> On run en détaché, un container nommé “bdd” et connecté au réseau “reseau” qui une fois stoppé meurt et les données sont gardés sur un volume 
($[...] -v name:path:options)


Création du dossier NGINX en avance


mkdir nginx


Container PHP


docker run -d --rm --name script --network reseau -v ./wordpress:/var/www/html php:8.2-apache


->On lance un container en détaché nommé “script” relié au réseau “reseau” qui une fois stoppé meurt et les données sont gardés sur un volume
-> La documentation permet d’obtenir l’arborescence 


Extension mysqli


docker exec script bash -c "docker-php-ext-configure mysqli;docker-php-ext-install mysqli;apachectl graceful"


-> Docker exec permet d’effectuer une commande sur un container
-> Les commandes sont sur la documentation mysqli
-> Permet la connection de wordpress


Récupération du fichier de configuration par défaut


docker run --rm nginx:1.24.0 cat /etc/nginx/conf.d/default.conf > ./nginx/default.conf


-> On copie le fichier de configuration et on le colle dans notre dossier NGINX créé auparavant 


cd nginx
Edition du fichier de configuration


vim default.conf


server {
    listen       80;
    server_name  serveur;


location / {
        proxy_pass http://SCRIPT:80;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }
}


-> On remplace le contenu du fichier avec le proxy pass


cd ../


docker run -d --rm --name http --network reseau -p 80:80 -v ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro nginx:1.24.0


-> On run en détaché, un container nommé “http” et connecté au réseau “reseau” (port 80) qui une fois stoppé meurt et les données sont gardés sur un volume
-> ro = read only pour empêcher la modification